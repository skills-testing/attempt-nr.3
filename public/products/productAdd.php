
<?php require_once('../../private/initialize.php');
?>

<?php
$args['SKU'] = $_POST['SKU'];
$args['Name'] = $_POST['Name'];
$args['Price']=$_POST['Price'];
//Height,Width,Length will be converted to the HxWxL
$args['Length']=$_POST['Length'];
$args['Width']=$_POST['Width'];
$args['Height']=$_POST['Height'];

$args['bookWeight']=$_POST['Weight'];
$args['discSize']=$_POST['Size'];
//Creating the new product
$product =  new ProductsContr($args);
//If it is the furniture data will be added (function also checks if there there the data exists)
$product->setDimensions();
//Creating the data with existing data
$product->createProduct();
header('Location: ../main.php');
