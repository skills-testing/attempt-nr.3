$(document).ready(function() {

    $('#btn_Chck').click(function() {
        $('input[id=child_chkbox]').prop('checked', true);
    });
}); 

// Type switcher<-------------------------------------------->
//Deleting Type Switcher value, because it will disturb in future data validation

$("#type_switch").click(function() {
    $("#type_switch option[value='new'").remove();
   });


function switcher() {
    var selectedOption = document.getElementById("type_switch").selectedIndex;
    var Options = document.getElementById("type_switch").options;
    var OptionText = Options[selectedOption].text;


    if (OptionText == "Discs") {
        document.getElementById('height').required = false;
        document.getElementById('width').required = false;
        document.getElementById('length').required = false;
        document.getElementById('weight').required = false;

        document.getElementById('size').required = true;
        document.getElementById("disks_add").style.display = "block";
        document.getElementById("furniture_add").style.display = "none";
        document.getElementById("books_add").style.display = "none";

    } else if (OptionText == "Furniture") {
        document.getElementById('size').required = false;
        document.getElementById('weight').required = false;

        document.getElementById('height').required = true;
        document.getElementById('width').required = true;
        document.getElementById('length').required = true;
        document.getElementById("disks_add").style.display = "none";
        document.getElementById("furniture_add").style.display = "block";
        document.getElementById("books_add").style.display = "none";

    } else if (OptionText == "Books") {
        document.getElementById('height').required = false;
        document.getElementById('width').required = false;
        document.getElementById('length').required = false;
        document.getElementById('size').required = false;

        document.getElementById('weight').required = true;
        document.getElementById("disks_add").style.display = "none";
        document.getElementById("furniture_add").style.display = "none";
        document.getElementById("books_add").style.display = "block";

    }

}
 //  var AllOptions = document.getElementsByTagName("option");
   
 function changeAction() {
    var x = document.getElementById("type_switch").selectedIndex;
    var action = document.getElementsByTagName("option")[x].value;
    if( action !=="") {
        document.getElementById("form_action").action = action;
    }else{}
}

/*
$( '#btn_Del' ).click( function() {
    var tmp = '';
    $( '#child_chkbox:checked' ).each( function() {
        // Do what you want...
        tmp += ' ' + $( this ).val();
    });
    alert( tmp );
});
*/





// ------------------------------------------------------------------------------------>

