<?php require_once('../private/initialize.php');?>
<?php include(SHARED_PATH . '/new_header.php'); ?>


<body>


    <form class="adding_form"  id = "form_action" method="POST" name="adding_form">
        <ul>
            <li>
                <h2>Product Add</h2>
                <button type="submit" id="btn_Apply" onclick="changeAction()">Save</button>
            </li>
            <hr style="height: 0.1em;background-color: black;">
            <li>
                <span class="require_notification">* Required field</span>
            </li>
            <li>
                <label for="SKU">SKU:</label>
                <input type="text" name="SKU" id="SKU" placeholder="SKU" required>
                <span class="form_hint">Enter the Stock Keeping Unit</span>
            </li>
            <li>
                <label for="name">Name:</label>
                <input type="text" name="Name" id="name" placeholder="Product Name" required>
                <span class="form_hint">Name of the price</span>
            </li>
            <li>
                <label for="price">Price:</label>
                
                   
                    <input type="number" name="Price" id="price" step="any" placeholder="&euro;" required>
                
            </li>
            <li>
                <label for="type_switch">Type Switcher</label>
                <select name="type_switch" id="type_switch" onchange="switcher()" onclick="deleteDefault()" required>
                    <option value="new">Type Switcher</option>
                    <option value="products/disks_add.php">Discs</option>
                    <option value="products/furniture_add.php">Furniture</option>
                    <option value="products/books_add.php">Books</option>
                </select>

            </li>

                <!-- <div id="element_add"></div> -->
               <p id="dem"></p> 
            <div id="books_add" value = "Books" style="display:none;">
                <li>

                    <label for="weight">Weight:</label>
                    <input type="number" name="Weight" id="weight" placeholder="Weight in kg" step="any">
                    <span class="form_hint">Enter the weight of books in kg</span>

                </li>
            </div>
            <div id="disks_add" value = "Discs" style="display:none;">
                <li>
                    <label for="size">Size:</label>
                    <input type="number" name="Size" id="size" placeholder="Size in MB">
                    <span class="form_hint">Enter the size of the disk in MB</span>
                </li>
            </div>
            <div id="furniture_add" value = "Furniture" style="display:none;">
                <li>
                    <label for="height">Height</label>
                    <input type="number" name="Height" id="height" placeholder="Height in cm" step="any">
                    <span class="form_hint">Enter the height of the furniture in cm</span>
                </li>

                <li>
                    <label for="width">Width</label>
                    <input type="number" name="Width" id="width" placeholder="Width in cm" step="any">
                    <span class="form_hint">Enter the width of the furniture in cm</span>
                </li>
                <li>
                    <label for="length">Length</label>
                    <input type="number" name="Length" id="length" placeholder="Length in cm" step="any">
                    <span class="form_hint">Enter the length of the furniture in cm</span>
                </li>
            </div>
        </ul>
       
    </form>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="assets/js/scripts.js"></script>
<?php include(SHARED_PATH . '/public_footer.php'); ?>