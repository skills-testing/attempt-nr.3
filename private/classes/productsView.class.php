<?php


class ProductsView extends Products {

    public function showProduct($Attribute) {
        $results = $this->getProduct($Attribute);
        for($i=0;$i<sizeof($results);$i++){

         ?>
         <label class="checkbox-label grid-item">
         <input type="checkbox" name= "id[]" id="child_chkbox" value="<?= $results[$i]['id'];?>" >
         <span class="checkbox-custom"></span>
          <p class="SKU"><?php echo $results[$i]['SKU']; ?></p>
         <h3 class="name"><?php echo $results[$i]['Name']?></h3>
         <p class="price">&euro;<?php echo $results[$i]['Price']; ?></p>
         <p class="spec_attr">
         <?php
         switch($Attribute){
             case 'furnitureDimension':
                echo "Dimensions: ".$results[$i][$Attribute]. " cm";
             break;
             case 'discSize':
                echo "Size: ".$results[$i][$Attribute]. " MB";
             break;
             case 'bookWeight':
                echo "Weight: ".$results[$i][$Attribute]. " kg";
             break;
             default:
             echo "Not defined!";
            break;

         }
        ?>
         </p>
         </label><?php
         
         
    }
    }

    

}



